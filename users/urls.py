from django.urls import path

from . import views

urlpatterns = [
    path('', views.get_users),
    path('create/', views.create_user),
    path('get/<int:id>', views.get_user_by_id),
    path('activate/<int:id>', views.activate),
    path('deactivate/<int:id>', views.deactivate),
]
import json

# from django.contrib.auth.models import User
from django.http import HttpResponse
from users.models import User
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods


@csrf_exempt
@require_http_methods("GET")
def get_users(request):
    users = User.objects.all()
    print(list(users))

    user_list = list(users)

    if (len(user_list) == 0):
        return HttpResponse("Nie ma uzytkownikow", status=404)

    data = serializers.serialize('json', users)
    return HttpResponse(data, content_type="application/json")


@csrf_exempt
@require_http_methods("POST")
def create_user(request):
    decoded_body = request.body.decode("utf-8")
    somejson = json.loads(decoded_body)
    email = somejson["email"]
    first_name = somejson["firstName"]
    last_name = somejson["lastName"]
    age = int(somejson["age"])
    pesel = somejson["pesel"]

    # user = User(email=email)
    user = User()
    user.email = email
    user.first_name = first_name
    user.last_name = last_name
    user.age = age
    user.pesel = pesel
    user.save()
    return HttpResponse("Create user", status=201)


@csrf_exempt
@require_http_methods("GET")
def get_user_by_id(request, id):
    users = User.objects.filter(id=id)

    if len(list(users)) == 0:
        return HttpResponse("User not found", status=404)

    # user = users[0]
    # data = serializers.serialize('json', users)
    #
    # return HttpResponse(data, status=200)

    user = users[0]

    response = [{
        "id": user.id,
        "email": user.email,
        "age": user.age,
        "firstName": user.first_name
    }]
    return HttpResponse(response, status=200, content_type=json)


@csrf_exempt  # bez tej adnotacji nie mozemy wykonywac zadan z postmana -> wylaczanie odpornosci na arak cross site request forgery
@require_http_methods("PATCH")  # ograniczamy mozliwosc wykonywania zadan do metody w "" PATCH to typ zadania z protokolem http
# GET  - nie moze wyslac danych /user/1
# POST - sluzy do tworzenia lub przesylania
# PUT - modyfikacja
# PATCH - modyfikacja
# DELETE - usuwanie zasobow
def activate(request, id):
    users = User.objects.filter(id=id)
    user_list = list(users)
    if len(user_list) == 0:
        return HttpResponse("Nie można aktywowac użytkownika. Uzytkownik nie istnieje", status=404)

    user = user_list[0]

    user.active = True
    user.save()
    if user.active == False:
        return HttpResponse("Użytkownik: {user.first_name jest już aktywowany", status=409)
    return HttpResponse(f"Aktywowano użytkownika: {user.first_name}", status=200)

@csrf_exempt
@require_http_methods("PATCH")
def deactivate(request, id):
    users = User.objects.filter(id=id)
    user_list = list(users)
    if len(user_list) == 0:
        return HttpResponse("Nie można zdezaktywowac użytkownika. Uzytkownik nie istnieje", status=404)

    user = user_list[0]

    if user.active == False:
        return HttpResponse("Użytkownik: {user.first_name jest już zdezaktywowany", status=409)

    user.active = False
    user.save()
    return HttpResponse(f"Zdezaktywowano użytkownika: {user.first_name}", status=200)


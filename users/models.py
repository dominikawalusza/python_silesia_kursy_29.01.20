from django.db import models


class User(models.Model):
    # Tworzenie zmiennej typu Integer będącej kluczem prywatnym
    id = models.IntegerField(primary_key=True, auto_created=True)

    email = models.CharField(max_length=100, unique=True)

    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)

    age = models.IntegerField()
    active = models.BooleanField(default=False)

    pesel = models.CharField(max_length=100, null=True)

    def __str__(self):
        return f"id: {self.id}," \
               f" email: {self.email}," \
               f" firstname: {self.first_name}" \
               f" lastname: {self.last_name}" \
               f" age: {self.age} " \
               f" active: {self.active}"